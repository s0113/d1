Git Commands

Git Bash Commands:
Cd ..   - Go back to root
mkdir - make directory (Format mkdir dirName)
cd - go to (Cd folder)
touch - create files (touch textname.txt)
pwd - print working directory

=====
SSH Key
=====

1. Windows
   open the "Windows Terminal" or "Git Bash"
2. Create an SSH
    ssh-keygen

3.  - Go to "Your public key has been saved in _path_"
   - Open rsa.pub file in sublime

4. Configure the git account in the device or project\
   COnfigure the global user email
   git confi --global user.email [git account email address]
   git config --global user.email "nicolematib@gmail.com"

5. Check the git user credentials 
   git config --global --list
   user.email=nicolematib@gmail.com
   user.name=nicole matib


COnfigure the global user names
     git config --global user.name "[git account username]"

. Configure project email 
   git confi --global user.email [git account email address]
   git config --global user.email "nicolematib@gmail.com"

cd Documents && cd batch-165

=============
Git Commands
=============
1. Initialize a local git repository
  $ git init
Initialized empty Git repository in C:/Users/MATIB_ISR 3_LC3/Documents/batch-165/s01/.git/

2. Peek at the states of the files/folders
git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        d1/

nothing added to commit but untracked files present (use "git add" to track)

3. Stage the file in preparation for creating a commit
	Staging files individually
	   git add [filename]
	      git add -A
4. Peek the states of the files/folders 
   git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   d1/discussion.txt

5. Create commit
   git commit -m

git commit -m "Initial Commit"
[master (root-commit) c6cf63c] Initial Commit
 1 file changed, 24 insertions(+)
 create mode 100644 d1/discussion.txt

6. Peek the states of the file
git status
On branch master
nothing to commit, working tree clean
